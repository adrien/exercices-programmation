# Exercices de programmation

Ces exercices en Python3 permettent d'apprendre progressivement les bases du langage, et de la programmation en général. 

Il ne s'agit que de commentaires proposant un énoncé et autres instructions/sources de documentation dans des fichiers vides.

On peut par exemple copier ces fichiers dans un répertoire de travail, et commencer à les remplir. Il ne faut pas hésiter à créer ses propres fichiers pour essayer quelques bouts de code, sans s'embêter de l'exercice courant. On peut aussi utiliser l'interpréteur interactif de Python pour faire des tests en console :

```python
$ python3 # Dans un terminal
[...]
>>> from random import randint
>>> randint(5)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: randint() missing 1 required positional argument: 'b'
>>> # Ah mince, il y a deux paramètres 
>>> randint(5, 10)
6 
>>> # quit(), exit() ou encore Ctrl-D pour quitter
```

Pour exécuter un fichier python, on ouvre un terminal, et on fait comme ça :

```bash
# D'abord, aller dans le répertoire du fichier python à exécuter
~ $ cd le/repertoire/du/fichier 
# J'étais dans ~ (mon répertoire personnel, par exemple /home/adrien)
# Je vais donc me retrouver dans ~/le/repertoire/du/fichier 
~/le/repertoire/du/fichier $ ls # Liste les fichiers
lama.jpg mon_code.py nul.jpg rien_a_faire.txt 
# C'est le fichier mon_code.py qu'on veut exécuter 
# On appelle python3, et on lui donne le nom du fichier en paramètre :
~/le/repertoire/du/fichier $ python3 mon_code.py 
[...] # Le fichier s'exécute 
# On peut aussi choisir de rendre le fichier exécutable pour le lancer différemment 
~/le/repertoire/du/fichier $ chmod +x mon_code.py 
~/le/repertoire/du/fichier $ ./mon_code.py 
```
