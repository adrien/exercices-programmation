#!/usr/bin/python3
# La ligne ci-dessus s'appelle un shebang sert à expliquer au système
# qu'on code en python. Autant la retenir par coeur ou la copier-coller...


# Énoncé : --------------------------------------------------------------------
# On cherche à écrire un programme du jeu de "pendu" :
#
# D'abord, l'ordinateur choisit un mot mystère parmi un fichier de mots
# Tant que l'utilisateur n'a pas gagné ou perdu :
#   L'ordinateur affiche le mot à trous et dit le nombre d'essais restants
#   L'utilisateur propose une lettre
#   Si la lettre appartient au mot
#     L'ordinateur révèle la lettre dans le mot à trou
#   Sinon
#     L'ordinateur retire un essai au joueur


# Fonctions additionnelles (optionnelles) : -----------------------------------
# * L'ordinateur dessine progressivement le pendu
# * L'ordinateur rappelle les lettres déjà proposées
# * Tableau des scores (on peut reprendre le code du "plus-ou-moins")


# Objectifs pédagogiques : ----------------------------------------------------
# * Manipuler des tableaux
# * Découper son code en fonctions
# * Faire de l'ASCII art (dessin du pendu)
# * Chaînes de caractères sur plusieurs lignes (dessin du pendu)


# Outils nécessaires : --------------------------------------------------------
# * Les tableaux
# mot = "pendu"
# lettres_manquantes = [True, False, True, True, False] # Un tableau de bool
# # Comment afficher le mot avec les trous ?
# Docs :
# https://python.doctor/page-apprendre-listes-list-tableaux-tableaux-liste-array-python-cours-debutant
# Trouvez de la doc
#
# * Les fonctions
# # Servent à écrire du code une fois, et à pouvoir l'appeler quand on veut :
# def afficher_mot_a_trous(mot, lettres_manquantes):
#     # ... Du code indenté ...
#
# # On peut maintenant appeler la fonction avec les paramètres qu'on veut
# afficher_mot_a_trous("pizza", [True, False, False, False, False])
# afficher_mot_a_trous("padakor", [True, False, True, False, True, True, True])
#
# * Un fichier de mots (fourni)
# Le fichier "mots_francais.txt" contient 22740 mots, donc des noms propres
# (qui commencent par une majuscule) et autres mots d'une lettre.
# Il va falloir trier !
