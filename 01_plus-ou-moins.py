#!/usr/bin/python3
# La ligne ci-dessus s'appelle un shebang sert à expliquer au système
# qu'on code en python. Autant la retenir par coeur ou la copier-coller...


# Énoncé : --------------------------------------------------------------------
# On cherche à écrire un programme du jeu "plus-ou-moins" :
#
# D'abord, l'ordinateur choisit un nombre mystère entre 1 et 100
# L'utilisateur propose un nombre, et l'ordinateur répond soit
# * "C'est plus" si le nombre mystère est supérieur à celui proposé
# * "C'est moins" si le nombre mystère est inférieur à celui proposé
# * "Gagné !" si l'utilisateur a trouvé le nombre mystère
# L'utilisateur doit reproposer un nombre tant qu'il n'a pas gagné


# Fonctions additionnelles (optionnelles) : -----------------------------------
# * Compter le nombre de coups avant victoire
# * Possibilité de choisir l'intervalle (par ex. 1 et 1000)
# * Tableau des scores (avec pseudonyme !)


# Objectifs pédagogiques : ----------------------------------------------------
# * Manipuler des variables
# * Boucles et conditions
# * Manipuler de fichiers (pour le tableau des scores)


# Outils nécessaires : --------------------------------------------------------
# * Selection aléatoire du nombre
# import random # Importe la bibliothèque (library) des fonctions aléatoires
# On utilise la fonction randint de la bibliothèque random
# (Et on met le résultat dans la variable x)
# x = random.randint(a, b)
# Lire la doc : https://docs.python.org/3.1/library/random.html#random.randint
# "Return a random integer N such that a <= N <= b."
#
# * Demander une valeur à l'utilisateur
# x = input()
# Lire la doc : https://docs.python.org/3.5/library/functions.html#input
# En fonction de ce que rentre l'utilisateur, x sera un int ou string ou autre
#
# * Manipuler des fichiers :
# Docs :
# https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/232431-utilisez-des-fichiers
# https://simple-duino.com/manipulation-de-fichiers-en-python/
# https://python.developpez.com/cours/apprendre-python3/?page=page_11
# Bref, trouvez de la doc.
#
#
# * Parler Python3 (syntaxe)
# Lire la doc : https://docs.python.org/3.5/reference/compound_stmts.html
# Ya des tutos moins arides :
#   * http://www.xavierdupre.fr/app/teachpyx/helpsphinx/c_lang/syntaxe.html
#   * https://docs.python.org/fr/3/tutorial/controlflow.html
#   * https://www.pierre-giraud.com/python-apprendre-programmer-cours/
#   * "tuto python3" dans google
#   * crier "ADRIEEEEEN !"
